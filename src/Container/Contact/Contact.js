import React from 'react';
import {Link} from "react-router-dom";
import './Contact.css'

const Contact = () => {
  return (
    <div className='Bg-About'>
      <div className="container">
        <nav>
          <ul className='Ul-nav'>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/AboutUs">About us</Link></li>
            <li><Link to="/Contact">Contact</Link></li>
          </ul>
        </nav>
        <div className='Border'>

          <h3>About Us</h3>
          <div>
            <p>Number: <strong>+(996)556-300-153</strong></p>
            <p>Secondary number: <strong>+(996)556-300-153</strong></p>
          </div>
          <div>
            <p>Location: <strong>Bishkek, st.Ahumbaeva 20</strong></p>
          </div>
          <div className='Back'>
            <a href="/" className='Back-About'>Back</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;