import React from 'react';
import {Link} from "react-router-dom";
import './AboutUs.css'

const AboutUs = () => {
  return (
    <div>
      <div className="container">
        <div>
          <nav>
            <ul className='Ul-nav'>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/AboutUs">About us</Link></li>
              <li><Link to="/Contact">Contact</Link></li>
            </ul>
          </nav>
        </div>
        <div className='text'>
          <h3>About us</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad adipisci
            consectetur corporis delectus ea facilis laboriosam non quaerat voluptate
            voluptates?
          </p>
        </div>

      </div>
    </div>
  );
};

export default AboutUs;