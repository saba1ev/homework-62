import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';

import './LandingPage.css'
class LandingPage extends Component {
  render() {
    return (
      <Fragment>
        <div className='Bg-header'>
          <div className='Opacity'>
            <header className='Header'>
              <div className="container">
                <div className="logo">
                  <Link to="/"/>
                </div>
                <nav>
                  <ul className='Ul-nav'>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/AboutUs">About us</Link></li>
                    <li><Link to="/Contact">Contact</Link></li>
                  </ul>
                </nav>
              </div>
            </header>
            <div className='container'>
              <h1 className='Title'>Сonstruction company</h1>
            </div>
          </div>
        </div>
      </Fragment> 
    );
  }
}

export default LandingPage;