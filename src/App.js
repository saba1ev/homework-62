import React, { Component } from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom'
import './App.css';
import LandingPage from "./Container/LandingPage/LandingPage";
import AboutUs from "./Container/AboutUs/AboutUs";
import Contact from "./Container/Contact/Contact";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path='/' exact component={LandingPage} />
            <Route path='/AboutUs' component={AboutUs}/>
            <Route path='/Contact' component={Contact}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
